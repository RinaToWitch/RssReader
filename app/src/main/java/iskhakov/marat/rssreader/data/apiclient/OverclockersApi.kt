package iskhakov.marat.rssreader.data.apiclient

import iskhakov.marat.rssreader.data.model.Rss
import kotlinx.coroutines.experimental.Deferred
import retrofit2.http.GET

interface OverclockersApi {

    @GET("news.rss")
    fun getNews(): Deferred<Rss>
}