package iskhakov.marat.rssreader.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementList
import org.simpleframework.xml.Root


@Root(name = "channel")
data class Channel(
        @get:Element(name = "title")
        @set:Element(name = "title")
        var title: String? = null,
        @get:Element(name = "description")
        @set:Element(name = "description")
        var description: String? = null,
        @get:Element(name = "link")
        @set:Element(name = "link")
        var link: String? = null,
        @get:Element(name = "lastBuildDate")
        @set:Element(name = "lastBuildDate")
        var lastBuildDate: String? = null,
        @get:ElementList(name = "item", inline = true)
        @set:ElementList(name = "item", inline = true)
        var item: List<Item>? = null,
        @get:Element(name = "image")
        @set:Element(name = "image")
        var image: Image? = null,
        @get:Element(name = "language")
        @set:Element(name = "language")
        var language: String? = null,
        @get:Element(name = "webMaster")
        @set:Element(name = "webMaster")
        var webMaster: String? = null
)
