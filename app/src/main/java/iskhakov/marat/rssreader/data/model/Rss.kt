package iskhakov.marat.rssreader.data.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Element
import org.simpleframework.xml.Root

@Root(name = "rss")
data class Rss(
        @get:Attribute(name = "version")
        @set:Attribute(name = "version")
        var version: String? = null,
        @get:Element(name = "channel")
        @set:Element(name = "channel")
        var channel: Channel? = null
) {

}