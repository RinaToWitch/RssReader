package iskhakov.marat.rssreader.data.di

import iskhakov.marat.rssreader.data.apiclient.ApiClient
import iskhakov.marat.rssreader.data.apiclient.OverclockersApi

interface ApiClientModule {

    val api: OverclockersApi

    class Impl : ApiClientModule {
        override val api: OverclockersApi
            get() = ApiClient.overclockersService
    }
}