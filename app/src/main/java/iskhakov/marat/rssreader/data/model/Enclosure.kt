package iskhakov.marat.rssreader.data.model

import org.simpleframework.xml.Attribute
import org.simpleframework.xml.Root

@Root(name = "enclosure")
class Enclosure(
        @get:Attribute(name = "type")
        @set:Attribute(name = "type")
        var type: String? = null,
        @get:Attribute(name = "url")
        @set:Attribute(name = "url")
        var url: String? = null
)
