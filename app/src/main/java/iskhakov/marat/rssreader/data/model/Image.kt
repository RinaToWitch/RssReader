package iskhakov.marat.rssreader.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.Root


@Root(name = "image")
data class Image(
        @get:Element(name = "title")
        @set:Element(name = "title")
        var title: String? = null,
        @get:Element(name = "height")
        @set:Element(name = "height")
        var height: String? = null,
        @get:Element(name = "description")
        @set:Element(name = "description")
        var description: String? = null,
        @get:Element(name = "link")
        @set:Element(name = "link")
        var link: String? = null,
        @get:Element(name = "width")
        @set:Element(name = "width")
        var width: String? = null,
        @get:Element(name = "url")
        @set:Element(name = "url")
        var url: String? = null
)
