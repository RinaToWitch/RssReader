package iskhakov.marat.rssreader.data.repository

import iskhakov.marat.rssreader.data.model.Item


interface NewsRepository {

    suspend fun getNews(): List<Item>

}
