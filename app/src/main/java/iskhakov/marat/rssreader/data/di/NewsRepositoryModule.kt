package iskhakov.marat.rssreader.data.di

import iskhakov.marat.rssreader.data.repository.NewsRepository
import iskhakov.marat.rssreader.data.repository.NewsRepositoryCloudImpl


interface NewsRepositoryModule {

    val newsRepository: NewsRepository

    class Impl(apiClientModule: ApiClientModule) : NewsRepositoryModule {
        override val newsRepository: NewsRepository by lazy {
            NewsRepositoryCloudImpl(apiClientModule.api)
        }

    }

}
