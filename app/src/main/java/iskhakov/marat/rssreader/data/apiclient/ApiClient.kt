package iskhakov.marat.rssreader.data.apiclient

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.simplexml.SimpleXmlConverterFactory


object ApiClient {
    var retrofit: Retrofit = Retrofit.Builder()
            .baseUrl("https://overclockers.ru/rss/")
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .addConverterFactory(SimpleXmlConverterFactory.create())
            .build()

    var overclockersService: OverclockersApi = retrofit.create(OverclockersApi::class.java)
}