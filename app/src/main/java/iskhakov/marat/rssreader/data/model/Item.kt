package iskhakov.marat.rssreader.data.model

import org.simpleframework.xml.Element
import org.simpleframework.xml.ElementMap
import org.simpleframework.xml.Root


@Root(name = "item")
data class Item(
        @get:ElementMap(entry = "guid", key = "isPermaLink", attribute = true, inline = true)
        @set:ElementMap(entry = "guid", key = "isPermaLink", attribute = true, inline = true)
        var guid: Map<Boolean, String>? = null,
        @get:Element(name = "pubDate")
        @set:Element(name = "pubDate")
        var pubDate: String? = null,
        @get:Element(name = "title")
        @set:Element(name = "title")
        var title: String? = null,
        @get:Element(name = "enclosure")
        @set:Element(name = "enclosure")
        var enclosure: Enclosure? = null,
        @get:Element(name = "description")
        @set:Element(name = "description")
        var description: String? = null,
        @get:Element(name = "link")
        @set:Element(name = "link")
        var link: String? = null
)
