package iskhakov.marat.rssreader.data.repository

import iskhakov.marat.rssreader.data.apiclient.OverclockersApi
import iskhakov.marat.rssreader.data.model.Item


class NewsRepositoryCloudImpl(val api: OverclockersApi) : NewsRepository {

    override suspend fun getNews(): List<Item> =
            api.getNews().await()
                    .channel?.item
                    ?: throw IllegalStateException()

}
