package iskhakov.marat.rssreader.core.di

import iskhakov.marat.rssreader.data.di.ApiClientModule
import iskhakov.marat.rssreader.data.di.NewsRepositoryModule


interface AppComponent :
        ApiClientModule,
        NewsRepositoryModule {

    class Impl(
            apiClientModule: ApiClientModule,
            private val newsRepositoryModule: NewsRepositoryModule
    ) : AppComponent,
            ApiClientModule by apiClientModule,
            NewsRepositoryModule by newsRepositoryModule


    interface NewsListSubcomponent : AppComponent, NewsListModule {

        class Impl(
                private val appComponent: AppComponent,
                private val newsListModule: NewsListModule
        ) : NewsListSubcomponent,
                AppComponent by appComponent,
                NewsListModule by newsListModule
    }
}
