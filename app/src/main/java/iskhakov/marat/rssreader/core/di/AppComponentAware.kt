package iskhakov.marat.rssreader.core.di


interface AppComponentAware {

    val applicationComponent: AppComponent

}
