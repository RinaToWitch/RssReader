package iskhakov.marat.rssreader.core

import android.app.Application
import iskhakov.marat.rssreader.core.di.DependencyContainer

class RssReaderApp : Application() {

    lateinit var dependencyContainer: DependencyContainer

    companion object {
        fun getAppInstance(application: Application): RssReaderApp =
                application as RssReaderApp
    }

    override fun onCreate() {
        super.onCreate()
        dependencyContainer = DependencyContainer()
    }
}