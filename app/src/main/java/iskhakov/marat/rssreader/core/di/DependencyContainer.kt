package iskhakov.marat.rssreader.core.di

import android.content.Context
import iskhakov.marat.rssreader.core.RssReaderApp
import iskhakov.marat.rssreader.data.di.ApiClientModule
import iskhakov.marat.rssreader.data.di.NewsRepositoryModule

class DependencyContainer {

    companion object {
        fun get(context: Context): DependencyContainer =
                (context.applicationContext as RssReaderApp).dependencyContainer
    }

    val appComponent: AppComponent

    init {
        val apiClientModule = ApiClientModule.Impl()
        val newsRepositoryModule = NewsRepositoryModule.Impl(apiClientModule)
        appComponent = AppComponent.Impl(apiClientModule, newsRepositoryModule)
    }

    var newsListSubcomponent: AppComponent.NewsListSubcomponent? = null

    fun plusNewsListModule(): AppComponent.NewsListSubcomponent {
        if (newsListSubcomponent == null) {
            newsListSubcomponent = AppComponent.NewsListSubcomponent.Impl(appComponent,
                    NewsListModule.Impl(appComponent))
        }
        return checkNotNull(newsListSubcomponent)
    }

    fun clearNewsListSubcomponent() {
        newsListSubcomponent = null
    }

}
