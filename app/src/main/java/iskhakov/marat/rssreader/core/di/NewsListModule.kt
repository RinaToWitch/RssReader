package iskhakov.marat.rssreader.core.di

import iskhakov.marat.rssreader.data.di.NewsRepositoryModule
import iskhakov.marat.rssreader.domain.interactor.NewsListInteractor
import iskhakov.marat.rssreader.presentation.NewsListPM

interface NewsListModule {

    val newsListPM: NewsListPM

    val newsListInteractor: NewsListInteractor

    class Impl(newsRepositoryModule: NewsRepositoryModule) : NewsListModule {
        override val newsListInteractor by lazy { NewsListInteractor.Impl(newsRepositoryModule.newsRepository) }
        override val newsListPM by lazy { NewsListPM(newsListInteractor) }
    }
}