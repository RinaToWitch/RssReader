package iskhakov.marat.rssreader.domain.interactor

import iskhakov.marat.rssreader.data.repository.NewsRepository
import iskhakov.marat.rssreader.presentation.model.NewsItem

interface NewsListInteractor {

   suspend fun getNews(): List<NewsItem>

    class Impl(private val newsRepository: NewsRepository): NewsListInteractor {

        override suspend fun getNews(): List<NewsItem> =
            newsRepository.getNews().map {
                NewsItem(it.title ?: "")
            }

    }

}