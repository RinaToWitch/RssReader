package iskhakov.marat.rssreader.domain.entity


data class News(
        val title: String
)
