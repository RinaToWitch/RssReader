package iskhakov.marat.rssreader.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import io.reactivex.disposables.Disposable
import iskhakov.marat.rssreader.R
import iskhakov.marat.rssreader.core.di.DependencyContainer
import iskhakov.marat.rssreader.presentation.model.NewsItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var newsListPM: NewsListPM

    val disposables: MutableList<Disposable> = arrayListOf()

    @SuppressLint("RxSubscribeOnError")
    override fun onCreate(savedInstanceState: Bundle?) {
        initDeps()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            newsListPM.onFirstViewCreated()
        }
        rv_items.layoutManager = LinearLayoutManager(this)
        disposables += newsListPM.news.subscribe {
            items: List<NewsItem> -> rv_items.adapter = ItemAdapter(items)
        }
    }

    private fun initDeps() {
        val newsListSubcomponent = DependencyContainer.get(this).plusNewsListModule()
        newsListPM = newsListSubcomponent.newsListPM
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.forEach { it.dispose() }
        if (isFinishing) {
            DependencyContainer.get(this).clearNewsListSubcomponent()
        }
    }
}


