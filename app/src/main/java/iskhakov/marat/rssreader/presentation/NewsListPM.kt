package iskhakov.marat.rssreader.presentation

import io.reactivex.subjects.BehaviorSubject
import iskhakov.marat.rssreader.domain.interactor.NewsListInteractor
import iskhakov.marat.rssreader.presentation.model.NewsItem
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async

class NewsListPM(private val newsListInteractor: NewsListInteractor) {

    val news: BehaviorSubject<List<NewsItem>> = BehaviorSubject.create()

    fun onFirstViewCreated() {
        async(UI) {
            try {
                val items = newsListInteractor.getNews()
                items.apply {
                    news.onNext(this)
                }
            } catch (ex: Exception) {

            }
        }
    }
}