package iskhakov.marat.rssreader.presentation.model

data class NewsItem(val title: String)